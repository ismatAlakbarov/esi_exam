package com.example.demo.inventory.application.dto;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

@Data
public class PlantReservationDTO extends ResourceSupport {
    Long _id;
    PlantInventoryItemDTO item;
    BusinessPeriodDTO schedule;
}
