package com.example.demo.inventory.application.service;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.dto.PlantReservationDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class PlantReservationAssembler
        extends ResourceAssemblerSupport<PlantReservation, PlantReservationDTO> {

    @Autowired
    PlantInventoryAssembler plantInventoryAssembler;

    public PlantReservationAssembler() {super(SalesRestController.class, PlantReservationDTO.class);}

    @Override
    public PlantReservationDTO toResource(PlantReservation plantReservation) {
        PlantReservationDTO dto = createResourceWithId(plantReservation.getId(), plantReservation);
        dto.set_id(plantReservation.getId());
        PlantInventoryItemDTO itemDto =  plantInventoryAssembler.toResource(plantReservation.getPlant());
        dto.setItem(itemDto);
        BusinessPeriodDTO businessPeriodDTO = BusinessPeriodDTO.of(plantReservation.getSchedule().getStartDate(), plantReservation.getSchedule().getEndDate());

        dto.setSchedule(businessPeriodDTO);
        return dto;
    }
}

