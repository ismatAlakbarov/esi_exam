package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.dto.PlantReservationDTO;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class PlantInventoryAssembler
        extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {


    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PlantInventoryAssembler() {
        super(SalesRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem item) {
        PlantInventoryItemDTO dto = createResourceWithId(item.getId(), item);
        dto.set_id(item.getId());
        dto.setSerialNumber(item.getSerialNumber());
        PlantInventoryEntryDTO entryDTO = plantInventoryEntryAssembler.toResource(item.getPlantInfo());
        dto.setPlantInfo(entryDTO);
        return dto;
    }
}
