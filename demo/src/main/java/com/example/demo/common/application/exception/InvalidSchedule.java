package com.example.demo.common.application.exception;

public class InvalidSchedule extends Exception  {

    public InvalidSchedule() {
        super("start date must be at least 5 days later than current date");
    }
}
